//
//  Stores.m
//  LocalList
//
//  Created by Karim Jiwani.
//  Copyright (c) 2013 Karim Jiwani. All rights reserved.
//

#import "Stores.h"


@implementation Stores

@dynamic storeName;
@dynamic storeAddress;
@dynamic storeReference;
@dynamic latitude;
@dynamic longitude;
@dynamic phoneNumber;
@dynamic priceLevel;
@dynamic storeWebsite;
@dynamic rating;

@end
