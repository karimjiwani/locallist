//
//  PlaceSearchTableViewController.m
//  LocalList
//
//  Created by Karim Jiwani.
//  Copyright (c) 2013 Karim Jiwani. All rights reserved.
//

#import "PlaceSearchTableViewController.h"
#import "PlaceService.h"
#import "Places.h"
#import "PlaceDetailTableViewController.h"

@interface PlaceSearchTableViewController ()

@end

@implementation PlaceSearchTableViewController {
    NSMutableArray *resultArray;
    PlaceService *placeService;
    NSMutableArray *keyArray;
    NSMutableArray *dataArray;
    CLLocationManager *locationManager;
    NSString *locationString;
}
@synthesize placeSearchBar = _placeSearchBar;
@synthesize tableView = _tableView;

static NSString *kLocation = @"location";
static NSString *kRadius = @"radius";
static NSString *kRankBy = @"rankby";
static NSString *kSensor = @"sensor";
static NSString *kName = @"name";
static NSString *kType = @"type";



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.placeSearchBar.delegate = self;
    self.tableView.delegate = self;
    resultArray = [[NSMutableArray alloc] initWithCapacity:0];
    locationManager = [[CLLocationManager alloc] init];
    [self startLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [NSString stringWithFormat:@"Total Results: %d", [resultArray count]];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [resultArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SearchStoreCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    Places *place = [resultArray objectAtIndex:indexPath.row];
    cell.textLabel.text = place.placeName;
    cell.detailTextLabel.text = place.placeAddress;
    return cell;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [segue.identifier isEqualToString:@"PlaceDetailSegue"] ) {
        NSIndexPath *indexPlath = [self.tableView indexPathForCell:sender];
        UINavigationController *navigationController = (UINavigationController *)segue.destinationViewController;
        PlaceDetailTableViewController *destinationController = (PlaceDetailTableViewController *)navigationController.topViewController;
        destinationController.placeService = placeService;
        destinationController.places = [resultArray objectAtIndex:indexPlath.row];
        destinationController.managedObjectContext = self.managedObjectContext;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    if ( ![CLLocationManager locationServicesEnabled] ) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Service Disbale" message:@"Please enbale location service from settings" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    } else if ([locationString isEqualToString:@"NA"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Accuracy" message:@"Please try again after few seconds" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }else {
        [resultArray removeAllObjects];
        [self generateQueryWithText:searchBar.text withNextPageToken:nil];
    }
}

- (void) generateQueryWithText:(NSString *)query withNextPageToken:(NSString *)nextPageToken {
    NSString *location = locationString;
    if ( nextPageToken == nil ) {
        [dataArray removeAllObjects];
        [keyArray removeAllObjects];
        keyArray = [[NSMutableArray alloc] initWithObjects:kLocation, kRankBy, kSensor, kName, kType, nil];
        dataArray = [[NSMutableArray alloc] initWithObjects:location, @"distance", @"true", query, @"grocery_or_supermarket", nil];
    } else {
        if ( [dataArray count] == 6 ) {
            [dataArray removeObjectAtIndex:5];
            [dataArray addObject:nextPageToken];
        } else {
            [keyArray addObject:@"pagetoken"];
            [dataArray addObject:nextPageToken];
        }
    }
    NSDictionary *queryData = [[NSDictionary alloc] initWithObjects:dataArray forKeys:keyArray];
    if ( placeService == nil ) {
        placeService = [[PlaceService alloc] init];
    }
    [placeService setPlaceQuery:queryData withSelector:@selector(displayQueryResults:) withDelegate:self];
}

- (void) forNextPage:(NSString *)nextPageToken {
    [self generateQueryWithText:nil withNextPageToken:nextPageToken];
}

- (void) displayQueryResults:(NSDictionary *)json {
    NSString *nextPageToken = nil;
    if ( [[json objectForKey:@"status"] isEqualToString:@"OK"] ) {
        nextPageToken = [json objectForKey:@"next_page_token"];
        NSArray *results = [json objectForKey:@"results"];
        for ( NSDictionary *result in results ) {
            Places *place = [[Places alloc] init];
            place.placeName = [result objectForKey:@"name"];
            place.placeAddress = [result objectForKey:@"vicinity"];
            place.latitude = [[[result objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"];
            place.longitude = [[[result objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"];
            place.reference = [result objectForKey:@"reference"];
            [resultArray addObject:place];

        }
    } else {
        NSLog(@"error getting data: %@", [json objectForKey:@"status"]);
    }
    [self.tableView reloadData];
    if ( nextPageToken != nil && [resultArray count] < 100 ) {
        [self performSelector:@selector(forNextPage:) withObject:nextPageToken afterDelay:1.5];
    }
}

#pragma marks Location Manager Method

- (void)startLocation {
    if ( [CLLocationManager locationServicesEnabled] ) {
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        [locationManager startUpdatingLocation];
    } else {
        [self performSelector:@selector(startLocation) withObject:nil afterDelay:5];
    }
}

- (void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    if ( [newLocation.timestamp timeIntervalSinceNow] < -5.0 ) {
        locationString = @"NA";
        return;
    }
    
    if ( newLocation.horizontalAccuracy < 0 ) {
        locationString = @"NA";
        return;
    }
    locationString = [self locationToString:newLocation];
}

- (NSString *)locationToString:(CLLocation *)location {
    return [NSString stringWithFormat:@"%f,%f", location.coordinate.latitude, location.coordinate.longitude];
}
@end
