//
//  SelectStoreTableViewController.m
//  LocalList
//
//  Created by Karim Jiwani.
//  Copyright (c) 2013 Karim Jiwani. All rights reserved.
//

#import "SelectStoreTableViewController.h"
#import "Stores.h"
@interface SelectStoreTableViewController ()

@end

@implementation SelectStoreTableViewController {
    NSArray *storesArray;
}

@synthesize managedObjectContext = _managedObjectContext, delegate = _delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self performFetch];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [storesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SelectStoreCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    Stores *store = [storesArray objectAtIndex:indexPath.row];
    cell.textLabel.text = store.storeName;
    cell.detailTextLabel.text = store.storeAddress;
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Stores *store = [storesArray objectAtIndex:indexPath.row];
    [self.delegate storeSelect:self withStoreName:store.storeName withStoreReference:store.storeReference];
}

- (void) performFetch {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Stores" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"storeName" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self loadStores:[self.managedObjectContext executeFetchRequest:fetchRequest error:nil]];
    });
}

- (void) loadStores:(NSArray *)resultArray {
    storesArray = resultArray;
    [self.tableView reloadData];
}
@end
