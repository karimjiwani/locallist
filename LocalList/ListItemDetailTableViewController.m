//
//  ListItemDetailTableViewController.m
//  LocalList
//
//  Created by Karim Jiwani.
//  Copyright (c) 2013 Karim Jiwani. All rights reserved.
//

#import "ListItemDetailTableViewController.h"
#import "List.h"
@interface ListItemDetailTableViewController ()

@end

@implementation ListItemDetailTableViewController {
    NSString *storeRef;
}

@synthesize itemNameLabel = _itemNameLabel, storeNameLabel = _storeNameLabel, quantityTextField = _quantityTextField, priceTextField = _priceTextField, toBuySwitch = _toBuySwitch, saveButton = _saveButton, managedObjectContext = _managedObjectContext, listItemToEdit = _listItemToEdit;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.quantityTextField.delegate = self;
    self.priceTextField.delegate = self;
    if ( self.listItemToEdit != nil ) {
        [self loadData];
    }
    [self checkSaveButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSString *decimalSymbol = [formatter decimalSeparator];
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString *expression = [NSString stringWithFormat:@"^([0-9]+)?(\\%@([0-9]{1,2})?)?$", decimalSymbol];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0,[newString length])];
    if (numberOfMatches == 0) {
        return NO;
    }
    return YES;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [segue.identifier isEqualToString:@"SelectItemSegue"] ) {
        SelectItemTableViewController *selectItemTableViewController = (SelectItemTableViewController *)segue.destinationViewController;
        selectItemTableViewController.delegate = self;
        selectItemTableViewController.managedObjectContext = self.managedObjectContext;
        
    } else if ( [segue.identifier isEqualToString:@"SelectStoreSegue"] ) {
        SelectStoreTableViewController *selectStoreTableViewController = (SelectStoreTableViewController *) segue.destinationViewController;
        selectStoreTableViewController.delegate = self;
        selectStoreTableViewController.managedObjectContext = self.managedObjectContext;
        
    }
}

- (void)checkSaveButton {
    if ( [self.itemNameLabel.text isEqualToString:@""] || [self.storeNameLabel.text isEqualToString:@""] ) {
        self.saveButton.enabled = NO;
    } else {
        self.saveButton.enabled = YES;
    }
}

- (void) closeScreen {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveItemInList:(UIBarButtonItem *)sender {
    List *listItem;
    if ( self.listItemToEdit != nil ) {
        listItem = self.listItemToEdit;
    } else {
        listItem = [NSEntityDescription insertNewObjectForEntityForName:@"List" inManagedObjectContext:self.managedObjectContext];
    }
    listItem.itemName = self.itemNameLabel.text;
    listItem.storeName = self.storeNameLabel.text;
    listItem.storeReference = storeRef;
    listItem.price = [NSDecimalNumber decimalNumberWithString:self.priceTextField.text];
    listItem.quantity = [NSDecimalNumber decimalNumberWithString:self.quantityTextField.text];
    listItem.toBuy = [NSNumber numberWithBool:[self.toBuySwitch isOn]];
    [self.managedObjectContext save:nil];
    [self closeScreen];
}

- (IBAction)cancelButton:(UIBarButtonItem *)sender {
    [self closeScreen];
}

- (void) loadData {
    self.itemNameLabel.text = self.listItemToEdit.itemName;
    self.storeNameLabel.text = self.listItemToEdit.storeName;
    self.priceTextField.text = [self.listItemToEdit.price stringValue];
    self.quantityTextField.text = [self.listItemToEdit.quantity stringValue];
    self.toBuySwitch.on = [self.listItemToEdit.toBuy boolValue];
    storeRef = self.listItemToEdit.storeReference;
}

- (void) itemSelect:(SelectItemTableViewController *)controller itemName:(NSString *)name {
    self.itemNameLabel.text = name;
    [self.navigationController popViewControllerAnimated:YES];
    [self checkSaveButton];
}

- (void) storeSelect:(SelectStoreTableViewController *)controller withStoreName:(NSString *)storeName withStoreReference:(NSString *)storeReference {
    self.storeNameLabel.text = storeName;
    storeRef = storeReference;
    [self.navigationController popViewControllerAnimated:YES];
    [self checkSaveButton];
}
@end
