//
//  List.m
//  LocalList
//
//  Created by Karim Jiwani.
//  Copyright (c) 2013 Karim Jiwani. All rights reserved.
//

#import "List.h"


@implementation List

@dynamic itemName;
@dynamic storeReference;
@dynamic quantity;
@dynamic price;
@dynamic toBuy;
@dynamic storeName;

@end
